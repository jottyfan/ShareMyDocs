package de.jottyfan.sharemydocs.app.common;

/**
 *
 * @author jotty
 *
 */
public class LambdaResultWrapper {
	private Integer counter;

	public LambdaResultWrapper() {
		counter = 0;
	}

	public void add(Integer i) {
		counter += i;
	}

	public Integer get() {
		return counter;
	}
}
