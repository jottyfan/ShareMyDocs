package de.jottyfan.sharemydocs.app.common;

import javax.servlet.http.HttpServletRequest;

import org.keycloak.KeycloakSecurityContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author jotty
 *
 */
@Service
public class SessionManager {

	/**
	 * try to get current keycloak user
	 *
	 * @param request the request
	 * @return the preferred username or null
	 */
	public String getCurrentUser(HttpServletRequest request) {
		KeycloakSecurityContext ksc = (KeycloakSecurityContext) request
				.getAttribute(KeycloakSecurityContext.class.getName());
		return ksc == null ? null : ksc.getIdToken().getPreferredUsername();
	}
}
