package de.jottyfan.sharemydocs.app.modules.document;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jotty
 *
 */
public class UploadBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private MultipartFile file;
	private Integer role;

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}
}
