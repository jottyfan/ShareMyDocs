package de.jottyfan.sharemydocs.app.modules.role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.jottyfan.sharemydocs.app.modules.document.DocumentBean;

/**
 *
 * @author jotty
 *
 */
public class RoleBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;

	private List<DocumentBean> documents;

	public RoleBean() {
		this.documents = new ArrayList<>();
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public List<DocumentBean> getDocuments() {
		return documents;
	}
}
