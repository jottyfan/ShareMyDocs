package de.jottyfan.sharemydocs.app.modules.role;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.jottyfan.sharemydocs.app.common.SessionManager;
import de.jottyfan.sharemydocs.app.modules.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class RoleController {

	@Autowired
	private RoleGateway gateway;

	@Autowired
	private SessionManager sessionManager;

	@Autowired
	private CommonController commonController;

	@GetMapping("/toAddrole")
	public String toAddRole(Model model) throws ServletException {
		model.addAttribute("role", new RoleBean());
		return "/role";
	}

	@PostMapping("/doAddrole")
	public String doAddRole(@ModelAttribute RoleBean bean, HttpServletRequest request, Model model) throws ServletException {
		boolean success = bean.getName().isBlank() ? false : gateway.addRole(bean, sessionManager.getCurrentUser(request));
		return success ? commonController.toIndex(model, request) : toAddRole(model);
	}

	@GetMapping("/deleteRole")
	public String doDeleteRole(@RequestParam(value = "id", required = true) Integer fkRole, HttpServletRequest request, Model model) {
		gateway.deleteRole(fkRole);
		return commonController.toIndex(model, request);
	}
}
