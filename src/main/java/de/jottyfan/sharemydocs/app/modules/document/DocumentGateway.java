package de.jottyfan.sharemydocs.app.modules.document;

import static de.jottyfan.sharemydocs.db.jooq.Tables.T_DOCUMENT;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_PROFILE;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_PROFILEROLE;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_ROLE;

import java.time.LocalDateTime;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertResultStep;
import org.jooq.InsertReturningStep;
import org.jooq.InsertValuesStep4;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.sharemydocs.app.common.LambdaResultWrapper;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TDocumentRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TProfileRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TProfileroleRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TRoleRecord;

/**
 *
 * @author jotty
 *
 */
@Repository
@Transactional(transactionManager = "transactionManager")
public class DocumentGateway {
	private static final Logger LOGGER = LogManager.getLogger(DocumentGateway.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * add the document to the database and add privileges for the user
	 *
	 * @param bean     the document bean
	 * @param username the user
	 * @return number of affected database rows
	 */
	public Integer addDocument(DocumentBean bean, String username) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			if (bean.getFkRole() == null) {
				InsertResultStep<TRoleRecord> sql0 = DSL.using(t)
				// @formatter:off
					.insertInto(T_ROLE,
							        T_ROLE.NAME)
					.values(UUID.randomUUID().toString())
					.returning(T_ROLE.PK);
				// @formatter:on
				LOGGER.debug(sql0.toString());
				bean.setFkRole(sql0.execute());
				lrw.add(1);
			}
			InsertReturningStep<TProfileRecord> sql1 = DSL.using(t)
			// @formatter:off
				.insertInto(T_PROFILE,
						        T_PROFILE.USERNAME)
				.values(username)
				.onConflict(T_PROFILE.USERNAME)
				.doNothing();
			// @formatter:on
			LOGGER.debug(sql1.toString());
			lrw.add(sql1.execute());

			InsertReturningStep<TProfileroleRecord> sql2 = DSL.using(t)
			// @formatter:off
				.insertInto(T_PROFILEROLE,
						        T_PROFILEROLE.FK_PROFILE,
						        T_PROFILEROLE.FK_ROLE)
				.select(DSL.using(t)
					.select(T_PROFILE.PK, DSL.val(bean.getFkRole()))
					.from(T_PROFILE)
					.where(T_PROFILE.USERNAME.eq(username)))
				.onConflict(T_PROFILEROLE.FK_PROFILE, T_PROFILEROLE.FK_ROLE)
				.doNothing();
			// @formatter:on
			LOGGER.debug(sql2.toString());
			lrw.add(sql2.execute());

			InsertValuesStep4<TDocumentRecord, LocalDateTime, String, Integer, byte[]> sql3 = DSL.using(t)
			// @formatter:off
				.insertInto(T_DOCUMENT,
						        T_DOCUMENT.CREATED,
						        T_DOCUMENT.NAME,
						        T_DOCUMENT.FK_ROLE,
						        T_DOCUMENT.DATA)
				.values(bean.getCreated(), bean.getName(), bean.getFkRole(), bean.getData());
			// @formatter:on
			LOGGER.debug(sql3.toString());
			lrw.add(sql3.execute());
		});
		return lrw.get();
	}
}
