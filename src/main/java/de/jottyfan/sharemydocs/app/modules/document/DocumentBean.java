package de.jottyfan.sharemydocs.app.modules.document;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @author jotty
 *
 */
public class DocumentBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String name;
	private LocalDateTime created;
	private LocalDateTime outdated;
	private Integer fkRole;
	private byte[] data;

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the created
	 */
	public LocalDateTime getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	/**
	 * @return the outdated
	 */
	public LocalDateTime getOutdated() {
		return outdated;
	}

	/**
	 * @param outdated the outdated to set
	 */
	public void setOutdated(LocalDateTime outdated) {
		this.outdated = outdated;
	}

	/**
	 * @return the fkRole
	 */
	public Integer getFkRole() {
		return fkRole;
	}

	/**
	 * @param fkRole the fkRole to set
	 */
	public void setFkRole(Integer fkRole) {
		this.fkRole = fkRole;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}
}
