package de.jottyfan.sharemydocs.app.modules;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.sharemydocs.app.common.SessionManager;
import de.jottyfan.sharemydocs.app.modules.role.RoleGateway;

/**
 *
 * @author jotty
 *
 */
@Controller
public class CommonController {

	@Autowired
	private RoleGateway roleGateway;

	@Autowired
	private SessionManager sessionManager;

	@GetMapping("/logout")
	public String doLogout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}

	/**
	 * navigate to index.html
	 * @return the index page
	 */
	@GetMapping("/")
	public String toIndex(Model model, HttpServletRequest request) {
		model.addAttribute("roles", roleGateway.getRolesOf(sessionManager.getCurrentUser(request)));
		return "index";
	}
}
