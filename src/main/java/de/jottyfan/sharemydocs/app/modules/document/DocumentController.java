package de.jottyfan.sharemydocs.app.modules.document;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.jottyfan.sharemydocs.app.common.SessionManager;
import de.jottyfan.sharemydocs.app.modules.CommonController;

/**
 *
 * @author jotty
 *
 */
@Controller
public class DocumentController {

	@Autowired
	private CommonController commonController;

	@Autowired
	private SessionManager sessionManager;

	@Autowired
	private DocumentGateway gateway;

	@GetMapping("/toAddDocument")
	public String toAddDocument(@RequestParam(value = "role", defaultValue = "") Integer role, Model model) {
		UploadBean bean = new UploadBean();
		bean.setRole(role);
		model.addAttribute("document", bean);
		return "/document";
	}

	@RequestMapping(path = "/doAddDocument", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public String doAddDocument(@ModelAttribute UploadBean uploadBean, Model model, HttpServletRequest request) throws IOException {
		DocumentBean bean = new DocumentBean();
		bean.setName(uploadBean.getFile().getName());
		bean.setData(uploadBean.getFile().getBytes());
		bean.setCreated(LocalDateTime.now());
		bean.setFkRole(uploadBean.getRole());
		gateway.addDocument(bean, sessionManager.getCurrentUser(request));
		return commonController.toIndex(model, request);
	}
}
