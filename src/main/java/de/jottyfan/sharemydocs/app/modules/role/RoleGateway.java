package de.jottyfan.sharemydocs.app.modules.role;

import static de.jottyfan.sharemydocs.db.jooq.Tables.T_DOCUMENT;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_FOLDER;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_PROFILE;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_PROFILEROLE;
import static de.jottyfan.sharemydocs.db.jooq.Tables.T_ROLE;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertReturningStep;
import org.jooq.Record2;
import org.jooq.SelectConditionStep;
import org.jooq.SelectSeekStep1;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.sharemydocs.app.common.LambdaResultWrapper;
import de.jottyfan.sharemydocs.app.modules.document.DocumentBean;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TDocumentRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TFolderRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TProfileRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TProfileroleRecord;
import de.jottyfan.sharemydocs.db.jooq.tables.records.TRoleRecord;

/**
 *
 * @author jotty
 *
 */
@Repository
@Transactional(transactionManager = "transactionManager")
public class RoleGateway {
	private static final Logger LOGGER = LogManager.getLogger(RoleGateway.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get the id of the user; if the user does not exist, create it
	 *
	 * @param dsl      the jooq context
	 * @param username the username
	 * @return the id of the user
	 */
	private Integer getFkProfile(Configuration dsl, String username) {
		InsertReturningStep<TProfileRecord> sql1 = DSL.using(dsl)
		// @formatter:off
			.insertInto(T_PROFILE,
					        T_PROFILE.USERNAME)
			.values(username)
			.onConflict(T_PROFILE.USERNAME)
			.doNothing();
		// @formatter:off
		LOGGER.debug(sql1.toString());
		sql1.execute();

		SelectConditionStep<TProfileRecord> sql2 = DSL.using(dsl).selectFrom(T_PROFILE).where(T_PROFILE.USERNAME.eq(username));
		LOGGER.debug(sql2.toString());
		return sql2.fetchOne().getPk();
	}

	/**
	 * add the new role to the database and add the user also to this role
	 *
	 * @param role     the role
	 * @param username the name of the user in t_profile
	 * @return true if successful
	 */
	public Boolean addRole(RoleBean bean, String username) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		jooq.transaction(t -> {
			Integer fkProfile = getFkProfile(t, username);
			InsertReturningStep<TRoleRecord> sql1 = DSL.using(t)
			// @formatter:off
				.insertInto(T_ROLE,
						        T_ROLE.NAME)
				.values(bean.getName())
				.onConflict(T_ROLE.NAME)
				.doNothing();
			// @formatter:on
			LOGGER.debug(sql1.toString());
			lrw.add(sql1.execute());

			InsertReturningStep<TProfileroleRecord> sql2 = DSL.using(t)
			// @formatter:off
				.insertInto(T_PROFILEROLE,
						        T_PROFILEROLE.FK_PROFILE,
						        T_PROFILEROLE.FK_ROLE)
				.select(DSL.using(t)
				  .select(DSL.val(fkProfile), T_ROLE.PK)
				  .from(T_ROLE)
				  .where(T_ROLE.NAME.eq(bean.getName())))
				.onConflict(T_PROFILEROLE.FK_PROFILE, T_PROFILEROLE.FK_ROLE)
				.doNothing();
			// @formatter:on
			LOGGER.debug(sql2.toString());
			lrw.add(sql2.execute());
		});
		return lrw.get() > 0;
	}

	/**
	 * get all roles of the user
	 *
	 * @param username the name of the user
	 * @return the roles of the user; an empty list at least
	 */
	public List<RoleBean> getRolesOf(String username) {
		SelectConditionStep<Record2<Integer, String>> sql = jooq
		// @formatter:off
			.select(T_ROLE.PK,
					    T_ROLE.NAME)
			.from(T_PROFILE)
			.leftJoin(T_PROFILEROLE).on(T_PROFILEROLE.FK_PROFILE.eq(T_PROFILE.PK))
			.leftJoin(T_ROLE).on(T_ROLE.PK.eq(T_PROFILEROLE.FK_ROLE))
			.where(T_PROFILE.USERNAME.eq(username));
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<RoleBean> list = new ArrayList<>();
		for (Record2<Integer, String> r : sql.fetch()) {
			RoleBean bean = new RoleBean();
			bean.setPk(r.get(T_ROLE.PK));
			bean.setName(r.get(T_ROLE.NAME));
			bean.getDocuments().addAll(getAllDocumentsOfRole(bean.getPk(), false));
			list.add(bean);
		}
		return list;
	}

	/**
	 * get all documents of the role
	 * @param fkRole the role Id
	 * @param includeData if true, load also its heavy data
	 * @return the list of documents; an empty one at least
	 */
	private List<DocumentBean> getAllDocumentsOfRole(Integer fkRole, Boolean includeData) {
		SelectSeekStep1<TDocumentRecord, String> sql = jooq
		// @formatter:off
			.selectFrom(T_DOCUMENT)
			.where(T_DOCUMENT.FK_ROLE.eq(fkRole))
			.orderBy(T_DOCUMENT.NAME);
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<DocumentBean> list = new ArrayList<>();
		for (TDocumentRecord r : sql.fetch()) {
			DocumentBean bean = new DocumentBean();
			bean.setCreated(r.getCreated());
			bean.setData(r.getData());
			if (includeData) {
				bean.setFkRole(fkRole);
			}
			bean.setName(r.getName());
			bean.setOutdated(r.getOutdated());
			bean.setPk(r.getPk());
			list.add(bean);
		}
		return list;
	}

	/**
	 * delete role and all the documents of it
	 *
	 * @param fkRole the Id of the role
	 */
	public void deleteRole(Integer fkRole) {
		jooq.transaction(t -> {
			DeleteConditionStep<TDocumentRecord> sql0 = DSL.using(t).deleteFrom(T_DOCUMENT).where(T_DOCUMENT.FK_ROLE.eq(fkRole));
			LOGGER.debug(sql0.toString());
			sql0.execute();

			DeleteConditionStep<TFolderRecord> sql1 = DSL.using(t).deleteFrom(T_FOLDER).where(T_FOLDER.FK_ROLE.eq(fkRole));
			LOGGER.debug(sql1.toString());
			sql1.execute();

			DeleteConditionStep<TProfileroleRecord> sql2 = DSL.using(t).deleteFrom(T_PROFILEROLE).where(T_PROFILEROLE.FK_ROLE.eq(fkRole));
			LOGGER.debug(sql2.toString());
			sql2.execute();

			DeleteConditionStep<TRoleRecord> sql3 = DSL.using(t).deleteFrom(T_ROLE).where(T_ROLE.PK.eq(fkRole));
			LOGGER.debug(sql3.toString());
			sql3.execute();
		});
	}
}
