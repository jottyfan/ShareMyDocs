class MyAjax {
	constructor(url) {
		this.url = url;
	}

	call(destdivid, needle) {
		$("[id='" + destdivid + "']").load(this.url + '?needle=' + encodeURI(needle));
	}
}