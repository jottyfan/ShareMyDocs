# CampOrganizer2

As a successor of the original https://gitlab.com/jottyfan/CampOrganizer , this application comes with current modern architectures to make development quicker and the user experience more smooth.

Breaking up the modules into several applications is way to heavy for our small server, so the decision was made to put it all into one single app.
